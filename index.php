<?php require_once "./code.php" ?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>S3: Classes, Objects, Inheritance, and Polymorphism</title>
</head>
<body>
	<h1>Objects from Variable</h1>

	<p><?php echo $buildingObj->name; ?></p>

	<!--  -->
	<h1>Objects From CLasses</h1>
	<p><?php var_dump($building); ?></p>
	<!-- var_dump if you want extra details -->
	<p><?php print_r($building->printName()); ?></p>

	<h1>Inheritance and Polymorphism</h1>

	<p><?php echo($building->printName()); ?></p>

	<p><?php echo($condominium->printName()); ?></p>

	<h1>Access Modifiers</h1>

	<h2>Building Variables</h2>
	<p><?php //echo $building->name; ?></p>

	<h2>Condominium Variables</h2>
	<p><?php //echo $condominium->name; ?></p>

	<h1>Encapsulation</h1>
	<p>The name of the condominium is <?php echo $condominium->getName(); ?></p>

	<p><?php $condominium->setName('Enzo Tower') ?></p>
	<p>The name of the condo has been changed to <?php echo $condominium->getName(); ?></p>

	<h1><em>Activity for Session 3</em></h1>

	<h1>Person</h1>
	<p><?php echo($person->printName()); ?></p>
	<p><?php echo($developer->printName()); ?></p>
	<p><?php echo($engineer->printName()); ?></p>

	<h1><em>Activity for Session 4</em></h1>
	<h1>Building</h1>
	<p>The name of the building is <?php echo $building->getName(); ?>.</p>
	<p>The <?php echo $building->getName();?> has  <?php echo $building->getFloors();?> floors.</p>
	<p>The <?php echo $building->getName();?> is located at <?php echo $building->getAddress();?>.</p>
	<p><?php $building->setName('Caswynn Complex') ?></p>
	<p>The name of the condo has been changed to <?php echo $building->getName(); ?></p>


	<h1>Condominium</h1>
	<p><?php $condominium->setName('Enzo Condo') ?></p>
	<p>The name of the condominium is <?php echo $condominium->getName(); ?>.</p>
	<p>The <?php echo $condominium->getName();?> has  <?php echo $condominium->getFloors();?> floors.</p>
	<p>The <?php echo $condominium->getName();?> is located at <?php echo $condominium->getAddress();?>.</p>
	<p><?php $condominium->setName('Enzo Tower') ?></p>
	<p>The name of the condo has been changed to <?php echo $condominium->getName(); ?></p>

</body>
</html>